package qontakgo

//QontakResponse - general Qontak responses
type QontakResponse struct {
	Meta struct {
		Status           int         `json:"status"`
		Type             string      `json:"type"`
		ErrorCode        interface{} `json:"error_code"`
		Info             string      `json:"info"`
		DeveloperMessage string      `json:"developer_message"`
		Message          string      `json:"message"`
		Timestamp        interface{} `json:"timestamp"`
		LogID            string      `json:"log_id"`
	} `json:"meta"`
	Response struct {
		Message string `json:"message"`
	} `json:"response"`
}
