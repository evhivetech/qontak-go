Go Qontak API Wrapper For cohive.space

```
    package main

    import (
        "log"

        qontakgo "bitbucket.org/evhivetech/qontak-go"
    )

    func main() {
        x, err := qontakgo.NewQontak("access token")

        if err != nil {
            log.Println(err.Error())
        }

        var jsonStr = []byte(`{"first_name":"Jo","last_name":"Massimiliano","email":"","building":"The Maja","phone_number":"","enquiry":"Flexi Desk City","source":"website"}`)

        err = x.NewLead(jsonStr)

        if err != nil {
            log.Println(err.Error())
        }
    }


```